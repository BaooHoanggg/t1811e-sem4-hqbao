package com.example.fullstack_rest.service;

import com.example.fullstack_rest.entity.Todo;
import com.example.fullstack_rest.repository.TodoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TodoService {
    private final TodoRepository todoRepository;

    @Autowired
    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<Todo> findAll() {
        return todoRepository.findAll();
    }

    public List<Todo> saveAll(List<Todo> todos) {
        return todoRepository.saveAll(todos);
    }

}
