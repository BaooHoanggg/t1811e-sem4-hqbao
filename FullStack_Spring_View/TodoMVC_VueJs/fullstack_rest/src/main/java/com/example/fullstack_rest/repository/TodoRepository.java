package com.example.fullstack_rest.repository;

import com.example.fullstack_rest.entity.Todo;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.Id;

public interface TodoRepository extends JpaRepository<Todo, Long> {
}
