package com.example.examead.repository;

import com.example.examead.entity.Candicate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CandicateRepository extends JpaRepository<Candicate, Long> {
}
