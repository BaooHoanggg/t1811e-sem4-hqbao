package com.example.examead.api;

import com.example.examead.entity.Candicate;
import com.example.examead.service.CandicateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/candicates")
public class CandicateAPI {
    private final CandicateService candicateService;

    @Autowired
    public CandicateAPI(CandicateService candicateService) {
        this.candicateService = candicateService;
    }

    @GetMapping
    public ResponseEntity<List<Candicate>> findAll() {
        return ResponseEntity.ok(candicateService.findAll());
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody Candicate candicate) {
        return ResponseEntity.ok(candicateService.save(candicate));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Candicate> findById(@PathVariable Long id) {
        Optional<Candicate> can = candicateService.findById(id);
        if (!can.isPresent()) {
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(can.get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Candicate> update(@PathVariable Long id,@Valid @RequestBody Candicate candicate) {
        Optional<Candicate> can = candicateService.findById(id);
        if (!can.isPresent()) {
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(candicateService.save(candicate));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Candicate> delete(@PathVariable Long id) {
        Optional<Candicate> can = candicateService.findById(id);
        if (!can.isPresent()) {
            ResponseEntity.badRequest().build();
        }
        candicateService.deleteById(id);
        return ResponseEntity.ok().build();
    }

}
