package com.example.examead.service;

import com.example.examead.entity.Candicate;
import com.example.examead.repository.CandicateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CandicateService {
    private final CandicateRepository candicateRepository;

    @Autowired
    public CandicateService(CandicateRepository candicateRepository) {
        this.candicateRepository = candicateRepository;
    }

    public List<Candicate> findAll() {
        return candicateRepository.findAll();
    }

    public List<Candicate> saveAll(List<Candicate> candicates) {
        return candicateRepository.saveAll(candicates);
    }

    public Optional<Candicate> findById(Long id) {
        return candicateRepository.findById(id);
    }

    public Candicate save(Candicate candicate) {
        return candicateRepository.save(candicate);
    }

    public void deleteById(Long id) {
        candicateRepository.deleteById(id);
    }
}
