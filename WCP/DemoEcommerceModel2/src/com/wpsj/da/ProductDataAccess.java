package com.wpsj.da;

import com.wpsj.entity.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class ProductDataAccess {
    private PreparedStatement searchStatement;

    private PreparedStatement getSearchStatement() throws ClassNotFoundException, SQLException {
        if(searchStatement == null) {
            Connection connection = new DBConnection().getConnection();
            searchStatement = connection.
                    prepareStatement("SELECT pro_id, pro_name,pro_desc FROM productstore WHERE" +
                            " pro_name like ?");
        }
        return searchStatement;
    }

    public List<Product> getProductByName(String name) {
        try {
            PreparedStatement statement = getSearchStatement();
            statement.setString(1, "%"+name+"%");
            ResultSet resultSet = statement.executeQuery();
            List<Product> products = new LinkedList<Product>();
            while (resultSet.next()) {
                products.add(new Product(resultSet.getInt("pro_id"),
                        resultSet.getString("pro_name"),
                        resultSet.getString("pro_desc")));
            }
            return products;
        }

        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
