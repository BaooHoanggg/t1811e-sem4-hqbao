package com.wpsj.model;

import com.wpsj.da.ProductDataAccess;
import com.wpsj.entity.Product;

import java.util.List;

public class ProductFinderBean {
    private String keyword;

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public List<Product> getProducts() {
        return new ProductDataAccess().getProductByName(keyword);
    }

}
