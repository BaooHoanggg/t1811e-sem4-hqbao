package com.wpsj.controller;

import com.wpsj.model.ProductFinderBean;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ProductFinder")
public class ProductFinder extends javax.servlet.http.HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {

        String keyword = request.getParameter("name");

        if (keyword == null || keyword.trim().isEmpty()) {
            response.sendRedirect("search.jsp?msg=The Search Bar Is Blank");
            return;
        }

        ProductFinderBean finder = new ProductFinderBean();
        finder.setKeyword(keyword);
        request.setAttribute("finder", finder);

        RequestDispatcher rd = request.getRequestDispatcher("result.jsp");
        rd.forward(request,response);
    }

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        processRequest(request,response);
    }


}
