<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: ASUS
  Date: 9/10/2020
  Time: 7:54 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:useBean id="listEmployee" class="com.example.controller.UserServlet" scope="session" />

<c:if test="${listEmployee == null}">
    <c:redirect url="employee.jsp" />
</c:if>

<head>
    <title>EMPLOYEE LIST</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
</head>
<body>

<div>
    <h1 style="text-align: center; color: #000000">USER LIST</h1>
</div>

<table class="table table-bordered">
    <thead>
    <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Birthday</th>
        <th>Address</th>
        <th>Position</th>
        <th>Department</th>
    </tr>
    </thead>

    <tbody>
    <c:set var="list" value="${requestScope.listEmployee}" scope="request" />
    <c:forEach var="employee" items="${list}">
        <tr>
            <td>
                <c:out value="${employee.id}" />
            </td>
            <td>
                <c:out value="${employee.fullname}" />
            </td>
            <td>
                <c:out value="${employee.birthday}" />
            </td>
            <td>
                <c:out value="${employee.address}" />
            </td>
            <td>
                <c:out value="${employee.positionn}" />
            </td>
            <td>
                <c:out value="${employee.department}" />
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>

<div>
    <form action="employee.jsp" style="color: green; margin: auto;">
        <input type="submit" value="ADD EMPLOYEE" />
    </form>
</div>

</body>
</html>
