package com.example.controller;

import com.example.bean.Employee;
import com.example.da.UserDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

@WebServlet("/AddingServlet")
public class AddingServlet extends HttpServlet {
    private UserDAO userDAO;

    @Override
    public void init() throws ServletException {
        userDAO = new UserDAO();
    }

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        //String action = request.getServletPath();
        try {
//            switch (action) {
//                case "/insert":
//                    insertUser(request, response);
//                    break;
//                default:
//                    listUser(request, response);
//                    break;
//            }
            insertUser(request,response);
        } catch (SQLException | ClassNotFoundException ex) {
            throw new ServletException(ex);
        }
    }

    private void insertUser(HttpServletRequest request, HttpServletResponse response)
            throws ClassNotFoundException, SQLException, IOException {
        String fullname = request.getParameter("fullname");
        String birthday = request.getParameter("birthday");
        String address = request.getParameter("address");
        String positionn = request.getParameter("positionn");
        String department = request.getParameter("department");
        Employee newEmployee = new Employee(fullname,birthday,address,positionn,department);
        userDAO.insertEmployee(newEmployee);
        response.sendRedirect("list");
    }
}
