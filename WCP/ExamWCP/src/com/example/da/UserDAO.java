package com.example.da;

import com.example.bean.Employee;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO {
    private String jdbcURL = "jdbc:mysql://localhost:3306/exam?useSSL=false";
    private String jdbcUsername = "root";
    private String jdbcPassword = "";

    private static final String INSERT_EMPLOYEES_SQL = "INSERT INTO employee" +
            " (fullname,birthday,address,positionn,department) VALUES" + " (?,?,?,?,?);";

    private static final String SELECT_ALL_EMPLOYEES = "SELECT * FROM employee;";

    public UserDAO(){

    }

    protected Connection getConnection()
            throws ClassNotFoundException, SQLException {
        Connection connection = null;
        Class.forName("com.mysql.cj.jdbc.Driver");
        connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        return connection;
    }

    public void insertEmployee(Employee employee)
            throws ClassNotFoundException, SQLException {
        Connection connection = getConnection();
        PreparedStatement preparedStatement = connection.
                prepareStatement(INSERT_EMPLOYEES_SQL);
        preparedStatement.setString(1,employee.getFullname());
        preparedStatement.setString(2,employee.getBirthday());
        preparedStatement.setString(3,employee.getAddress());
        preparedStatement.setString(4,employee.getPositionn());
        preparedStatement.setString(5,employee.getDepartment());
        preparedStatement.executeUpdate();
    }

    public List<Employee> selectAllEmployees()
            throws ClassNotFoundException, SQLException {
        List<Employee> employees = new ArrayList<>();
        Connection connection = getConnection();
        PreparedStatement preparedStatement = connection.
                prepareStatement(SELECT_ALL_EMPLOYEES);
        ResultSet rs = preparedStatement.executeQuery();

        while (rs.next()) {
            int id = rs.getInt("id");
            String fullname = rs.getString("fullname");
            String birthday = rs.getString("birthday");
            String address = rs.getString("address");
            String positionn = rs.getString("positionn");
            String department = rs.getString("department");
            employees.add(new Employee(id,fullname,birthday,address,positionn,department));
        }
        return employees;
    }

}
