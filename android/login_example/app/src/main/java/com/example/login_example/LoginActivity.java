package com.example.login_example;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText edUser;
    private EditText edPass;
    private Button btLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        edUser = (EditText)findViewById(R.id.edUser);
        edPass = (EditText)findViewById(R.id.edPass);
        btLogin = (Button)findViewById(R.id.btLogin);
        btLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btLogin:
                onLogin();
                break;
            default:
                break;
        }
    }

    private void onLogin() {
        if ((edUser.getText().toString().isEmpty()) || (edPass.getText().toString().isEmpty())) {
            Toast.makeText(this, "Username or Password is empty",
                    Toast.LENGTH_SHORT).show();
        }

        else {
            Intent intent = new Intent(this, ProfileActivity.class);
            intent.putExtra("USER_NAME", edUser.getText().toString());
            startActivity(intent);
        }
    }
}

















