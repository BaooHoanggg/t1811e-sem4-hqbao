package com.example.login_example;

import android.content.Intent;
import android.os.Bundle;
import android.service.autofill.OnClickAction;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class EditUserActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText edUser;
    private Button btDone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_user);
        edUser = (EditText)findViewById(R.id.edUser);
        btDone = (Button)findViewById(R.id.btDone);
        btDone.setOnClickListener(this);

        Intent intent = getIntent();
        String username = intent.getStringExtra("USER_NAME");
        edUser.setText(username);
    }
    
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btDone:
                onDone();
                break;
            default:
                break;
        }
    }

    private void onDone() {

    }

}
