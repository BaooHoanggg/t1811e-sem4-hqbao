package com.example.recycle_view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Product> listProduct = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //B1: Data Source
        initData();

        //B2: Adapter
        ProductAdapter adapter = new ProductAdapter(this, listProduct);

        //B3: Layout Manager
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 2);

        //B4: Recycle View
        RecyclerView rvProduct = findViewById(R.id.rvProduct);
        rvProduct.setLayoutManager(layoutManager);
        rvProduct.setAdapter(adapter);
    }

    private void initData() {
        listProduct.add(new Product("Nike Jordan 1", "200$", R.drawable.sp1));
        listProduct.add(new Product("Nike AirForce 1", "150$", R.drawable.sp2));
        listProduct.add(new Product("Nike Kyrie 5 SpongeBob", "200$", R.drawable.sp3));
        listProduct.add(new Product("Nike Air Mag", "1500$", R.drawable.sp4));
        listProduct.add(new Product("Nike Jordan 1 OffWhite Blue", "1000$", R.drawable.sp5));
        listProduct.add(new Product("Nike Air Supreme", "500$", R.drawable.sp6));
    }
}













