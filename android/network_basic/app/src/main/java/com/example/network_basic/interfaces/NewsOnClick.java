package com.example.network_basic.interfaces;

public interface NewsOnClick {
    void onClickItem(int position);
}
