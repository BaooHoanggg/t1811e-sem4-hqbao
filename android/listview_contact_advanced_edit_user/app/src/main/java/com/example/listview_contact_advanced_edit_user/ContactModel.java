package com.example.listview_contact_advanced_edit_user;

public class ContactModel {
    private String name;
    private String number;
    private int Image;

    public ContactModel(String name, String number, int image) {
        this.name = name;
        this.number = number;
        Image = image;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public int getImage() {
        return Image;
    }
}
