package com.example.listview_contact_advanced_edit_user;

public interface IOnChildItemClick {
    void onItemChildClick(int position);
}
