package com.example.listview_contact_advanced_edit_user;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;

import java.util.List;

public class ContactAdapter extends BaseAdapter {
    private Context mContext;
    private List<ContactModel> listContact;
    private IOnChildItemClick iOnChildItemClick;

    public ContactAdapter(Context mContext, List<ContactModel> listContact) {
        this.mContext = mContext;
        this.listContact = listContact;
    }

    public void registerChildItemClick(IOnChildItemClick iOnChildItemClick) {
        this.iOnChildItemClick = iOnChildItemClick;
    }

    public void unRegisterChildItemClick() {
        this.iOnChildItemClick = null;
    }

    //the number of items in data set
    @Override
    public int getCount() {
        return listContact.size();
    }

    //get data with the position in data set
    @Override
    public Object getItem(int position) {
        return null;
    }

    //get the row id of the position in data set
    @Override
    public long getItemId(int position) {
        return 0;
    }

    //get a view to display data at the position in data set
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        //reuse View
        if (rowView == null) {
            LayoutInflater inflater = ((Activity)mContext).getLayoutInflater();
            rowView = inflater.inflate(R.layout.item_contact, null);

            //configure view holder
            ViewHolder holder = new ViewHolder();
            holder.tvName = (TextView) rowView.findViewById(R.id.tvName);
            holder.tvNumber = (TextView) rowView.findViewById(R.id.tvNumber);
            holder.ivAvatar = (ImageView) rowView.findViewById(R.id.ivAvatar);
            holder.ibtCall = (ImageButton) rowView.findViewById(R.id.ibtCall);
            holder.ibtEdit = (ImageButton) rowView.findViewById(R.id.ibtEdit);

            rowView.setTag(holder);
        }

        //fill data
        ViewHolder holder = (ViewHolder) rowView.getTag();
        holder.tvName.setText(listContact.get(position).getName());
        holder.tvNumber.setText(listContact.get(position).getNumber());
        holder.ivAvatar.setImageResource(listContact.get(position).getImage());

        holder.ibtCall.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                onCall(position);
            }
        });

        holder.ibtEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iOnChildItemClick.onItemChildClick(position);
            }
        });

        return rowView;

    }

    static class ViewHolder {
        TextView tvName;
        TextView tvNumber;
        ImageView ivAvatar;
        ImageButton ibtCall;
        ImageButton ibtEdit;
    }

    private void onCall(int position) {
        ContactModel contact = listContact.get(position);
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("number: " + contact.getNumber()));

        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.CALL_PHONE)
            != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mContext.startActivity(intent);
    }
}




















