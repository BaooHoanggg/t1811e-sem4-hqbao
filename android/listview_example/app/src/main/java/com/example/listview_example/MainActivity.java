package com.example.listview_example;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ListView lvContact;
    private List<ContactModel> listContacts = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();

//        //B1: Data source
//        final String[] datas = {"SAMSUNG", "APPLE", "LG", "SONY", "KIA",
//                "SAMSUNG", "APPLE", "LG", "SONY", "KIA",
//                "SAMSUNG", "APPLE", "LG", "SONY", "KIA"
//        };
//
//        //B2: Data Adapter
//        ArrayAdapter<String> adapter =
//                new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,datas);
//
//        //B3: ListView
//        ListView lvUser = findViewById(R.id.lvContact);
//        lvUser.setAdapter(adapter);
//        lvUser.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String data = datas[position];
//                Toast.makeText(MainActivity.this, data, Toast.LENGTH_SHORT).show();
//            }
//        });

        lvContact =  (ListView) findViewById(R.id.lvContact);
        ContactAdapter adapter = new ContactAdapter(listContacts, this);
        lvContact.setAdapter(adapter);

        lvContact.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ContactModel contactModel = listContacts.get(position);
                Toast.makeText(MainActivity.this, contactModel.getName(),
                        Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void initData() {
        ContactModel contact = new ContactModel("Mr.Anonymous 1",
                "???-???-???",
                R.drawable.hacker1);
        listContacts.add(contact);

        contact = new ContactModel("Mr.Anonymous 2", "???-???-???", R.drawable.hacker1);
        listContacts.add(contact);
        contact = new ContactModel("Mr.Anonymous 3", "???-???-???", R.drawable.hacker1);
        listContacts.add(contact);
        contact = new ContactModel("Mr.Anonymous 4", "???-???-???", R.drawable.hacker1);
        listContacts.add(contact);
        contact = new ContactModel("Mr.Anonymous 5", "???-???-???", R.drawable.hacker1);
        listContacts.add(contact);
        contact = new ContactModel("Mr.Anonymous 6", "???-???-???", R.drawable.hacker1);
        listContacts.add(contact);
        contact = new ContactModel("Mr.Anonymous 7", "???-???-???", R.drawable.hacker1);
        listContacts.add(contact);
        contact = new ContactModel("Mr.Anonymous 8", "???-???-???", R.drawable.hacker1);
        listContacts.add(contact);
        contact = new ContactModel("Mr.Anonymous 9", "???-???-???", R.drawable.hacker1);
        listContacts.add(contact);
        contact = new ContactModel("Mr.Anonymous 10", "???-???-???", R.drawable.hacker1);
        listContacts.add(contact);
        contact = new ContactModel("Mr.Anonymous 11", "???-???-???", R.drawable.hacker1);
        listContacts.add(contact);
        contact = new ContactModel("Mr.Anonymous 12", "???-???-???", R.drawable.hacker1);
        listContacts.add(contact);
        contact = new ContactModel("Mr.Anonymous 13", "???-???-???", R.drawable.hacker1);
        listContacts.add(contact);
        contact = new ContactModel("Mr.Anonymous 14", "???-???-???", R.drawable.hacker1);
        listContacts.add(contact);
        contact = new ContactModel("Mr.Anonymous 15", "???-???-???", R.drawable.hacker1);
        listContacts.add(contact);
    }
}













