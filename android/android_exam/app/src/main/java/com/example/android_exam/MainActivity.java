package com.example.android_exam;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText edName;
    private EditText edEmail;
    private Spinner spin;
    private EditText edContent;
    private CheckBox cbResponse;
    private Button btSend;
    public String type;
    private TextView tvCount;
    AppDatabase db;

    private String [] dataSpinner = {"type 1", "type 2", "type 3"};

    public Feedback feedback = new Feedback();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        db = AppDatabase.getAppDatabase(this);

        edName = findViewById(R.id.edName);
        edEmail = findViewById(R.id.edEmail);
        spin = findViewById(R.id.spin);
        edContent = findViewById(R.id.edContent);
        cbResponse = findViewById(R.id.cbResponse);
        btSend = findViewById(R.id.btSend);
        tvCount = findViewById(R.id.tvCount);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                dataSpinner);

        adapter.setDropDownViewResource
                (android.R.layout.simple_list_item_single_choice);
        spin.setAdapter(adapter);

        spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                feedback.type = dataSpinner[position];
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }
        });

        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                feedback.name = edName.getText().toString();
                feedback.email = edEmail.getText().toString();
                feedback.content = edContent.getText().toString();
                feedback.response = cbResponse.isChecked();

                db.feedbackDAO().insertFeedback(feedback);
                tvCount.setText(db.feedbackDAO().getCount());

            }
        });

    }
}