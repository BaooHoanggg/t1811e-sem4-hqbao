package com.example.android_exam;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "feedBack")
public class Feedback {

    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "email")
    public String email;

    @ColumnInfo(name = "type")
    public String type;

    @ColumnInfo(name = "content")
    public String content;

    @ColumnInfo(name = "response")
    public Boolean response;
}
