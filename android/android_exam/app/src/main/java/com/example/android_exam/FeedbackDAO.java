package com.example.android_exam;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface FeedbackDAO {
    @Insert(onConflict = REPLACE)
    void insertFeedback(Feedback feedback);

    @Query("SELECT COUNT(*) FROM feedBack")
    Integer getCount();
}
