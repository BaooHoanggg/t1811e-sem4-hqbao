$(document).ready(function(){
    getAllProducts();
});

//Lấy Danh sách Product
function getAllProducts() {
    $.ajax({
        url: 'localhost:9999/api/v1/products',
        type: 'GET',
        dataType: 'json',
        contentType: 'application/json',
        success: function (res) {
            $("#tbl-pro-list").html('');

            $.each(res.data, function (index, data) {
                let html = '<tr>' +
                    '<td>' + data.id + '</td>' +
                    '<td>' + data.name  + '</td>' +
                    '<td>' + data.price  + '</td>' +
                    '<td>' + data.quantity  + '</td>' +
                    '<td><button type="button" class="btn btn-danger" onclick="sellProduct(' + data.id + ')">Sell this Product</button></td>' +
                    '</tr>';

                $('#tbl-pro-list').append(html);
            });
        },
        error: function (res) {
            alert("Get Product List Error!");
        },
    });
}

//Thêm Product
function addProduct() {
    let name = $('#add-p-name').val();
    let price = $('#add-p-price').val();
    let quantity = $('#add-p-quantity').val();

    let newProduct = {
        "name": name,
        "price": price,
        "quantity": quantity
    }

    console.log(newProduct);

    let newProductJson = JSON.stringify(newProduct);

    $.ajax({
        url: 'localhost:9999/api/v1/products',
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        data: newProductJson,
        success: function (res) {
            alert("Add Product Success!");
        },
        error: function (res) {
            alert("Add Product Error!");
        },
    });

    //Cập nhật lại Danh sách Product
    getAllProducts();

    //Làm trống các trường vừa nhập
    $('#add-p-name').val('');
    $('#add-p-price').val('');
    $('#add-p-quantity').val('');
}

