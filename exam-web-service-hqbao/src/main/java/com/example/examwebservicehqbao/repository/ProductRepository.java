package com.example.examwebservicehqbao.repository;

import com.example.examwebservicehqbao.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Long> {
    //Product findByIdAndAndQuantity(Long id);
}
