package com.example.examwebservicehqbao.service;

import com.example.examwebservicehqbao.entity.Product;
import com.example.examwebservicehqbao.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService {
    private final ProductRepository productRepository;

    @Autowired
    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public List<Product> findAll() {
        return productRepository.findAll();
    }

    public Optional<Product> findById(Long id) {
        return productRepository.findById(id);
    }

    public Product save(Product pro) {
        return productRepository.save(pro);
    }

//    public Product updateProductQuantity(Long id, Integer quantity) {
//        Product product = productRepository.findByIdAndAndQuantity(id);
//        product.setQuantity(quantity);
//        return productRepository.save(product);
//    }

    public void deleteById(Long id) {
        productRepository.deleteById(id);
    }
}
