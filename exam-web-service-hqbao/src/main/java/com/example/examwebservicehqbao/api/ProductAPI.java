package com.example.examwebservicehqbao.api;

import com.example.examwebservicehqbao.entity.Product;
import com.example.examwebservicehqbao.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/products")
public class ProductAPI {
    private final ProductService productService;

    @Autowired
    public ProductAPI(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping
    public ResponseEntity<List<Product>> findAll() {
        return ResponseEntity.ok(productService.findAll());
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody Product product) {
        return ResponseEntity.ok(productService.save(product));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Product> findById(@PathVariable Long id) {
        Optional<Product> pro = productService.findById(id);
        if (!pro.isPresent()) {
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(pro.get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<Product> update(@PathVariable Long id,@Valid @RequestBody Product product) {
        Optional<Product> pro = productService.findById(id);
        if (!pro.isPresent()) {
            ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok(productService.save(product));
    }

//    @PutMapping("/update-quantity/{id}")
//    public ResponseEntity<Product> updateQuantity(@PathVariable Long id,
//                                                  @Valid @RequestBody Integer quantity) {
//        Optional<Product> pro = productService.findById(id);
//        if (!pro.isPresent()) {
//            ResponseEntity.badRequest().build();
//        }
//
//        return ResponseEntity.ok(productService.updateProductQuantity(id, quantity));
//
//        //return ResponseEntity.ok(productService.save(product));
//    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Product> delete(@PathVariable Long id) {
        Optional<Product> pro = productService.findById(id);
        if (!pro.isPresent()) {
            ResponseEntity.badRequest().build();
        }
        productService.deleteById(id);
        return ResponseEntity.ok().build();
    }

}
