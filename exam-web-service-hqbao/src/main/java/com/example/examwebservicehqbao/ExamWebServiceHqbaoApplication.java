package com.example.examwebservicehqbao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExamWebServiceHqbaoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExamWebServiceHqbaoApplication.class, args);
    }

}
