package com.example.exam.model;

import com.example.exam.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;

public interface UserModel extends PagingAndSortingRepository<User, Integer> {
    Page<User> findUserById(int id, Pageable pageable);

    Page<User> findUserByNameContains(String name, Pageable pageable);

    Page<User> findUserByAge(int age, Pageable pageable);

    Page<User> findUserBySalary(long salary, Pageable pageable);

    Page<User> findUserByStatus(int status, Pageable pageable);
}
