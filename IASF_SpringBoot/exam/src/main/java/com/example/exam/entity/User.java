package com.example.exam.entity;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "users", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "This field cannot be blank")
    @Size(min = 3, message = "User's name should be longer than 3 characters")
    @Column(name = "name")
    private String name;

    @NotBlank(message = "This field cannot be blank")
    @Size(min = 18, message = "User's age must be 18 or older")
    @Column(name = "age")
    private int age;

    @Column(name = "salary")
    private long salary;

    private int status; // 1 - Hired / 0 - Erased

    public User() {
        this.status = 1;
    }

    public User(@NotBlank(message = "This field cannot be blank") @Size(min = 3, message = "User's name should be longer than 3 characters") String name, @NotBlank(message = "This field cannot be blank") @Size(min = 18, message = "User's age must be 18 or older") int age, long salary, int status) {
        this.name = name;
        this.age = age;
        this.salary = salary;
        this.status = 1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public long getSalary() {
        return salary;
    }

    public void setSalary(long salary) {
        this.salary = salary;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
