package com.example.exam.entity;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class UserValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User st = (User) o;
        if (st.getName().equals("Tuân")) {
            errors.rejectValue("name", null, "This User cannot be added for " +
                    "privacy reasons");
        }
    }
}
