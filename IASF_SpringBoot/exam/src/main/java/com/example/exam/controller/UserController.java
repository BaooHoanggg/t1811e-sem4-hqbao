package com.example.exam.controller;

import com.example.exam.entity.User;
import com.example.exam.entity.UserValidator;
import com.example.exam.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;


@Controller
public class UserController {


    @Autowired
    private UserModel userModel;

    @RequestMapping(path = "/user/create", method = RequestMethod.GET)
    public String createProduct(@ModelAttribute User u) {
        return "user-form";
    }

    @RequestMapping(path = "/user/create", method = RequestMethod.POST)
    public String saveProduct(@Valid User user, BindingResult result) {
        new UserValidator().validate(user, result);

        if (result.hasErrors()) {
            return "user-form";
        }

        userModel.save(user);
        return "redirect:/user/list";
    }

    @RequestMapping(path = "/user/edit/{id}", method = RequestMethod.GET)
    public String editProduct(@PathVariable int id, Model model) {
        Optional<User> optionalProduct = userModel.findById(id);
        if (optionalProduct.isPresent()) {
            model.addAttribute("user", optionalProduct.get());
            return "user-form";
        } else {
            return "not-found-form";
        }
    }

    @RequestMapping(path = "/user/edit/{id}", method = RequestMethod.POST)
    public String updateProduct(@PathVariable int id,@Valid User user, BindingResult result, Model model){
        Optional<User> optionalProduct = userModel.findById(id);
        if (optionalProduct.isPresent()) {
            if (result.hasErrors()) {
                return "user-form";
            }
            userModel.save(user);
            return "redirect:/user/list";
        } else {
            return "not-found-form";
        }
    }

    @RequestMapping(path = "/user/delete/{id}", method = RequestMethod.GET)
    public String deleteProduct(@PathVariable int id) {
        Optional<User> optionalProduct = userModel.findById(id);
        if (optionalProduct.isPresent()) {
            userModel.delete(optionalProduct.get());
            return "redirect:/user/list";
        } else {
            return "not-found-form";
        }
    }

    @RequestMapping(path = {"/user/list", "/"}, method = RequestMethod.GET)
    public String getListProduct(Model model, @RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "10") int limit) {
        Page<User> pagination = userModel.findUserByStatus(1, PageRequest.of(page - 1, limit));
        model.addAttribute("pagination", pagination);
        model.addAttribute("page", page);
        model.addAttribute("limit", limit);
        return "user-list";
    }

    @RequestMapping(path = "/user/search", method = RequestMethod.GET)
    public String searchByName(Model model, @RequestParam String name, @RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "10") int limit) {
        Page<User> pagination = userModel.findUserByNameContains(name, PageRequest.of(page - 1, limit));
        model.addAttribute("pagination", pagination);
        model.addAttribute("page", page);
        model.addAttribute("limit", limit);
        return "user-list";
    }



}
