package com.example.exam.endpoint;

import com.example.exam.entity.CustomErrorType;
import com.example.exam.entity.User;
import com.example.exam.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Optional;

public class UserEndpoint {
    @Autowired
    private UserModel userModel;

    @RequestMapping(path = "/endpoint/user/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity delete(@PathVariable int id) {
        Optional<User> optionalProduct = userModel.findById(id);
        if (optionalProduct.isPresent()) {
            User user = optionalProduct.get();
            user.setStatus(0);
            userModel.save(user);
            return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity(new CustomErrorType("Unable to delete. Product with id " + id + " not found."),
                    HttpStatus.NOT_FOUND);
        }
    }
}
