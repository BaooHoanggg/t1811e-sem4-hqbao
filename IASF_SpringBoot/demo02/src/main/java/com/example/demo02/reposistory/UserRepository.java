package com.example.demo02.reposistory;

import com.example.demo02.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    //Biz Method

}
